﻿import socket
import xml.parsers.expat
import sys
import os
from lxml.etree import tostring, fromstring
import re

if sys.version_info >= (3, 0):
    import configparser as ConfigParser
else:
    import ConfigParser
import json

my_format = "<?xml version=\"1.0\"?><wordsegmentation version=\"0.1\" charsetcode=\"utf-8\"><option showcategory=\"1\"/>%s<text>%s</text></wordsegmentation>"

# config = ConfigParser.ConfigParser()

# config.read(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'config.ini')
# authentication_string = "<authentication username=\"%s\" password=\"%s\"/>" % (
#     config.get("Authentication", "Username"), config.get("Authentication", "Password"))
# connect_target = config.get("Server", "IP"), int(config.get("Server", "Port"))
authentication_string = "<authentication username=\"%s\" password=\"%s\"/>" % ('_tester', 'tester')
# connect_target = 'localhost', 1501
# connect_target = ('140.96.89.87', 1501)
connect_target=('140.96.176.205',1501)

class parse_xml:
    def __init__(self, input_xml_str):
        self.status_code, self.status_str, self.result = None, '', ''
        self.core = xml.parsers.expat.ParserCreate('utf-8')
        self.core.StartElementHandler = self.start_element
        self.core.EndElementHandler = self.end_element
        self.core.CharacterDataHandler = self.char_data
        self.pointer = None
        if type(input_xml_str) is str:
            self.core.Parse(input_xml_str.strip(), 1)
        else:
            self.core.Parse(input_xml_str.encode('utf-8').strip(), 1)

    def start_element(self, name, attrs):
        if name == "processstatus":
            self.status_code = int(attrs['code'])
            self.pointer = name
        elif name == "sentence":
            self.pointer = name

    def end_element(self, name):
        if name == "wordsegmentation":
            self.result = self.result.strip()

    def char_data(self, data):
        if self.pointer is None:
            return None
        if self.pointer == "processstatus":
            self.status_str = data
        elif self.pointer == "sentence":
            self.result += data
        self.pointer = None


def ckip_client(input_text, output_file=None):
    input_text = input_text.replace('　', ' ').strip()
    input_text = input_text.replace('&', '&amp;')
    input_text = input_text.replace('<', '&lt;')
    input_text = input_text.replace('>', '&gt;')
    input_text = input_text.replace('\'', '&apos;')
    input_text = input_text.replace('"', '&quot;')

    text = my_format % (authentication_string, input_text)
    if sys.version_info >= (3, 0) and len(text) >= 7900:
        print('Input text is too long :', len(text))
        return None
    # raise ValueError("Your input text is too long.")
    elif sys.version_info < (3, 0) and len(text.decode('utf-8')) >= 7900:
        print('Input text is too long :', len(text))
        return None
    # raise ValueError("Your input text is too long.")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(connect_target)
    try:
        sock.sendall(text)
        downloaded = ''
        stop_word = "</wordsegmentation>"
    except:
        sock.sendall(text.encode('utf-8'))
        downloaded = b''
        stop_word = b"</wordsegmentation>"
    while stop_word not in downloaded:
        chunk = sock.recv(4096)

        downloaded += chunk
    result = parse_xml(downloaded.decode('utf-8'))
    # print(downloaded.decode('utf-8'))  #這邊可以看回傳結果(xml格式)
    ####fix tag error####
    result_tree = fromstring(downloaded)
    sentences = result_tree.iterfind('./result/sentence')
    result_raw = ''
    for sentence in sentences:
        result_raw += sentence.text
    result_list = result_raw.split('\u3000')
    result_new = ' '.join([raw for raw in result_list if raw != ''])
    ####fix tag error####

    if result.status_code == 0:
        if output_file:
            output = open(output_file, 'wb')
            output.write(result.result.encode('utf-8'))
            output.close()
        try:
            return result_new, len(text.decode('utf-8'))
        # return result.result, len(text.decode('utf-8'))
        except:
            return result_new, len(text)
            # return result.result, len(text)
    else:
        class CKIPException(Exception):
            pass

    print('CKIP error')
    return None
    # raise CKIPException("status_code: %d, %s" % (result.status_code, result.status_str))


def raw2ckip(inp):
    # 正規詞性列表
    posList = ['a', 'b', 'caa', 'cab', 'cba', 'cbb', 'da', 'dfa', 'dfb', 'di', 'dk', 'd', 'na', 'nb', 'nc', 'ncd', 'nd',
               'neu', 'nes', 'nep', 'neqa', 'neqb', 'nf', 'ng', 'nh', 'nv', 'i', 'p', 't', 'va', 'vac', 'vb', 'vc',
               'vcl', 'vd', 've', 'vf', 'vg', 'vh', 'vhc', 'vi', 'vj', 'vk', 'vl', 'v_2', 'de', 'shi', 'fw',
               'commacategory', 'dashcategory', 'etccategory', 'exclamationcategory', 'parenthesiscategory',
               'pausecategory', 'periodcategory', 'questioncategory', 'semicoloncategory', 'spchangecategory']
    inp = inp.replace('\xa0', '').replace('\u3000', '')

    # sentences=re.split('\n|。', inp)
    split_list = ['\n', '。']  # 用來切割的符號
    sentences = []
    inp_part = ''
    for char in inp:
        if char in split_list:  # 出現切割符號
            if char != '\n':  # 不是換行才要補
                inp_part += char
            sentences.append(inp_part)
            inp_part = ''
        else:
            inp_part += char
    if inp_part != '':
        sentences.append(inp_part)

    spaceFilter = re.compile(r'\s{1,}')  # 檢查空白
    all_term = []
    all_pos = []
    for sentence in sentences:
        part_term = []
        part_pos = []
        res = spaceFilter.sub('', sentence)
        if len(res) != 0:
            result = ckip_client(sentence)
            pat = re.compile(r'\([0-9,A-Z,a-z,_]+\)')
            if result == None:
                pass
                # return None
            else:
                for tp in result[0].split(' '):
                    result_re = pat.findall(tp)
                    if result_re != None and len(result_re) == 1:
                        pos = result_re[0]
                        part_term.append(tp.replace(pos, ''))
                        part_pos.append(pos.replace('(', '').replace(')', ''))
                        # all_term.append(tp.replace(pos, ''))
                        # all_pos.append(pos.replace('(', '').replace(')', ''))
                    elif result_re != None and len(result_re) > 1:
                        print(tp, result_re)
                        for p in result_re:
                            new_term = tp.split(p)[0]
                            termPos = p.replace('(', '').replace(')', '')
                            if termPos.lower() not in posList:  # 括弧內是否為正規詞性
                                print('not match', p)
                                continue
                            part_term.append(new_term)
                            part_pos.append(p.replace('(', '').replace(')', ''))
                            # all_term.append(new_term)
                            # all_pos.append(p.replace('(', '').replace(')', ''))
                            tp = tp.replace(new_term, '', 1).replace(p, '', 1)
                if part_term != [] and part_pos != []:
                    all_term.append(part_term)
                    all_pos.append(part_pos)
                # part_term = []
                # part_pos = []
        else:
            pass
    return all_term, all_pos


if __name__ == "__main__":
    # text = "Facebook是一個聯繫朋友、工作夥伴、同學或其他社交圈之間的社交工具。你可以利用Facebook 與朋友保持密切聯絡，無限量地上傳相片，分享轉貼連結及影片。"
    text = '＃標題＃重點規格一次看！ Apple iMac Pro 工作站等級電...Apple 在今年 WWDC 全球開發者大會上，發表全新 iMac Pro，連同搭配的鍵盤滑鼠，全面採用太空灰配色，全新的 iMac Pro 屬於工作站等級電腦，將搭載 Intel Xeon 處理器、ECC 內存記憶體、3GB/s SSD 固態硬碟等硬體規格配置。iMac Pro 還將搭載最新一代的 macOS High Sierra 作業系統，並搭載 5K Retina 顯示螢幕，並首次採用 10bit 面板與 500nits 螢幕亮度配置，預計將於今年 12 月正式販售。現場公佈的 iMac Pro 重點規格，還包含 1080p FaceTime 視訊鏡頭、UHS-II SDXC 記憶卡讀卡機、DDR4 2666MHz ECC 記憶體、30bit 色彩支援等規格。採用雙風扇散熱系統。最高支援到 Intel 18 核心 Xeon 處理器，另外還有 8 核心或 10 核心處理器版本可選，Turbo Boost 最高可達 4.2GHz，並支援最高 42GB 快取記憶體。內建 AMD 最新的 Radeon Pro Vega 顯示卡，最高具備 16GB 獨立顯示記憶體，並支援最高 400GB/s 記憶體讀寫速度。最高可支援到 22 萬億次浮點運算效能。最高支援 128GB DDR4-2666MHz ECC 內存記憶體。最高 4TB 儲存容量 3GB/s SSD 固態硬碟。機身背後的 I/O 端子配置，則可以看到 3.5mm 音源插孔、UHS-II SDXC 記憶卡讀卡機、四組 USB 3.0 連接埠、四組 USB-C Tunderbolt 3 傳輸埠。最後則是特別強調的 10GB 高速傳輸的 RJ-45 有線網路插孔。Thunderbolt 3 傳輸埠可支援兩個 5K 外接顯示螢幕，以及 2 個 NAS 儲存系統。Apple iMac Pro 擁有支援 VR 遊戲體驗、真實 4K 影像聲音效果等功能。具備 Retina 5K 顯示螢幕、8 核心 Xeon 處理器、Radeon Vega 顯示卡、32GB ECC 內存記憶體、1TB SSD 硬體規格的 iMac Pro，官方建議售價是 4,999 美元，折合台幣約為 151,270 元新台幣，這還是未稅的價格。Apple iMac Pro 將於今年 12 月在美國上市。歡迎加入小惡魔FB粉絲團：http://www.facebook.com/pages/Mobile01/189979853379'
    result = raw2ckip(text)
    print(result)
    # print(result[0])
    # print(result[0].split('\u3000'))
    # for term in result[0].split(' '):
    #     print(term, len(term))
