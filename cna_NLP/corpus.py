import os
# from collections import defaultdict
from pprint import pprint
import re, codecs, json
import traceback

#改成正面表列篩選stopword
def get_stopword():
    if not os.path.exists('ckip_stopword.json'):
        return [], []
    else:
        infile = json.load(codecs.open('ckip_stopword.json', 'r', 'utf-8'))
        return infile['term'], infile['pos']


def remove_symbol(string):
    symbol_pattern = re.compile("["
                                u"\U00000021-\U0000002E"  # !~.
                                u"\U0000003A-\U00000040"  # :~@   
                                u"\U0000005B-\U00000060"  # [~`
                                u"\U0000007B-\U00002E49"
                                u"\U00002FF0-\U000033FF"  # 表意象符號、中日韓符號標點    
                                u"\U00004DC0-\U00004DFF"  # 易經符號
                                u"\U0000A000-\U0000D7FF"  # 彝文、韓文等外文
                                u"\U0000FB00-\U0000FF0F"  # 字母表達形式、阿拉伯字母、豎排、組合用符號
                                u"\U0000FF1A-\U0000FF20"  # 全形符號
                                u"\U0000FF3B-\U0000FF40"  # 全形符號
                                u"\U0000FF5B-\U0000FFFF"
                                u"\U00010000-\U000E01EF"
                                "]+", flags=re.UNICODE)
    return symbol_pattern.sub(r'', string)


def dir_dic():
    news_cate = {}
    for path, dir_list, file_list in os.walk(os.path.join(os.path.dirname(__file__), 'CKIP')):
        for file_name in file_list:
            catename = os.path.abspath(os.path.join(path, file_name)).split(os.sep)[-2]
            if catename not in news_cate:
                news_cate[catename] = []

            if re.match('2018-0[2-4].json', file_name, flags=0) != None:
                file = os.path.abspath(os.path.join(path, file_name))
                news_cate[catename].extend([file])

    # pprint(news_cate)
    return news_cate


if __name__ == '__main__':
    news_dic = dir_dic()

    stopword, stoppos = get_stopword()

    open('corpus.txt', 'w', encoding='utf-8')
    debug_dic = dict()
    for cate_num, category in enumerate(news_dic):
        print('=' * 40)
        print('輸出' + category)

        corpus = open('corpus.txt', 'a', encoding='utf-8')
        corpus.write(category + ' ')

        cate_sum = 0  # 單一類別下的資料集總數
        file_sum = len(news_dic[category])
        if file_sum is 0:
            continue
        for json_file in news_dic[category]:
            print(json_file)
            segedfile = json.load(codecs.open(json_file, 'r', 'utf-8'))
            cate_sum += len(segedfile['Articles'])

        print('總計\t' + str(cate_sum) + '筆資料')
        for json_file in news_dic[category]:
            debug_dic[json_file] = dict()
            segedfile = json.load(codecs.open(json_file, 'r', 'utf-8'))
            extrac_count = len(segedfile['Articles'])  # 檔案內所有文件皆拿來輸出
            # extrac_count=500

            file_serial = 0  # 檔案序數
            for i in range(0, 1 + int(extrac_count / len(segedfile['Articles']))):
                for article_id in segedfile['Articles'].keys():
                    debug_dic[json_file][article_id] = []
                    file_serial += 1
                    try:
                        title = list(
                            zip([item for row in segedfile['Articles'][article_id]['Title_wordform'] for item in row],
                                [item for row in segedfile['Articles'][article_id]['Title_pos'] for item in row]))
                        content = list(
                            zip([item for row in segedfile['Articles'][article_id]['Content_wordform'] for item in row],
                                [item for row in segedfile['Articles'][article_id]['Content_pos'] for item in row]))

                    except Exception as e:
                        print(json_file, str(article_id), str(e))
                    # 增加標題權重
                    clean_wordform = []
                    for wordform, pos in [item for filed in (title, content) for item in filed]:
                        if wordform not in stopword and pos not in stoppos and len(remove_symbol(wordform)) > 0:
                            if wordform.find('\t') != '-1':  # 有時候CKIP斷詞完畢之後會有很怪異的「'\t我'」之類的字詞
                                wordform = wordform.replace('\t', '')
                            clean_wordform.extend(wordform)
                            corpus.write(wordform + ' ')
                        # else:
                        #     print(pos)
                    corpus.write('\n')

        corpus.write('\n')
        with codecs.open('debug.json', mode='w', encoding='utf-8-sig') as output:
            json.dump(debug_dic, output, ensure_ascii=False, indent=4)
