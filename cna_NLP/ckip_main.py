# encoding=utf-8
import codecs
import glob
import json
import os
import collections
from collections import Counter
from collections import defaultdict
from CKIP_client import raw2ckip
from pprint import pprint
import re
import traceback


def stopword():
    stopword_list = json.load(codecs.open('ckip_stopword.json', 'r', 'utf-8-sig'))
    return stopword_list


def remove_symbol(string):
    symbol_pattern = re.compile("["
                                u"\U00000021-\U0000002E"  # !~.
                                u"\U0000003A-\U00000040"  # :~@   
                                u"\U0000005B-\U00000060"  # [~`
                                u"\U0000007B-\U00002E49"
                                u"\U00002FF0-\U000033FF"  # 表意象符號、中日韓符號標點    
                                u"\U00004DC0-\U00004DFF"  # 易經符號
                                u"\U0000A000-\U0000D7FF"  # 彝文、韓文等外文
                                u"\U0000FB00-\U0000FF0F"  # 字母表達形式、阿拉伯字母、豎排、組合用符號
                                u"\U0000FF1A-\U0000FF20"  # 全形符號
                                u"\U0000FF3B-\U0000FF40"  # 全形符號
                                u"\U0000FF5B-\U0000FFFF"
                                u"\U00010000-\U000E01EF"
                                "]+", flags=re.UNICODE)
    return symbol_pattern.sub(r'', string)


def remove_urls(string):
    string = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', string, flags=re.MULTILINE)
    return string


# def remove_url(string):
def json_to_ckip(path, ckip_path):
    """
        generate a file with segmented result
        :param path: json file
    """
    segedfile = defaultdict(dict)

    with open(path, 'r', encoding='utf-8') as f:
        jsonfile = json.load(f)
        segedfile['Source_type'] = jsonfile['Source_type']
        segedfile['Source_name'] = jsonfile['Source_name']
        segedfile['Board_name'] = jsonfile['Board_name']
        segedfile['Date_range'] = jsonfile['Date_range']
        segedfile['Articles'] = defaultdict(dict)

        for articleID in jsonfile['Articles']:
            try:
                segedfile['Articles'][articleID]['Title_wordform'], \
                segedfile['Articles'][articleID]['Title_pos'] = raw2ckip(jsonfile['Articles'][articleID]['Title'])
                segedfile['Articles'][articleID]['Content_wordform'], \
                segedfile['Articles'][articleID]['Content_pos'] = raw2ckip(jsonfile['Articles'][articleID]['Content'])
                segedfile['Articles'][articleID]['Date']=jsonfile['Articles'][articleID]['Date']
                segedfile['Articles'][articleID]['Time']=jsonfile['Articles'][articleID]['Time']
                segedfile['Articles'][articleID]['Author']=jsonfile['Articles'][articleID]['Author']

                # import re
                # # remove url in Detail
                # detail = jsonfile['Articles'][articleID]['Detail']
                # detail = re.sub(r'//(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', detail,
                #                 flags=re.MULTILINE)
                # detail = re.sub(r'http:|https:|\xa0|\t', '', detail)
                #
                # segedfile['Articles'][articleID]['Detail_wordform'], \
                # segedfile['Articles'][articleID]['Detail_pos'] = raw2ckip(detail)
            except Exception as e:
                print(articleID, traceback.format_exc())
    if not os.path.exists(os.path.dirname(ckip_path)):
        os.mkdir(os.path.dirname(ckip_path))
    with open(os.path.abspath(ckip_path), 'w', encoding='utf-8') as output:
        json.dump(segedfile, output, indent=4, ensure_ascii=False)


def newline_join_list(biglist=list, symbol=str):
    """
    :param biglist: a double-layered list
    :return:a list have symbol
    """
    # print(biglist)
    for index, sublist in enumerate(biglist):
        if index < len(biglist) - 1:
            sublist.append(symbol)

    flat_list = [item for sublist in biglist for item in sublist]
    # print(flat_list)
    return flat_list


def check_segfile():
    """看資料夾下是否每個爬蟲檔都有斷詞檔"""
    jsonfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'cna_realtime', '*', '*.json'))
    ckipfiles = \
        glob.glob(os.path.join(os.path.dirname(__file__), 'cna_realtime', '*', '*_ckip.json'))
    # segfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'PChome 購物中心', '*', '*_seg.json'))
    # segfiles.extend(ckipfiles)
    ecfiles = list(set(jsonfiles).difference(set(ckipfiles)))  # 差集：segfiles not in the jsonfiles
    print(ecfiles)

    for ecfile in ecfiles:
        # check the file of segment result. if not, use json_to_ckip function to generate it.
        segfile = os.path.splitext(os.path.abspath(ecfile))[0] + '_ckip.json'

        if not os.path.exists(segfile):
            print(os.path.split(segfile)[-1], 'segmentation result does not exist.')
            json_to_ckip(ecfile)


def check_ckipfile():
    jsonfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'cna_realtime', '*',
                                       '[0-9][0-9][0-9][0-9]-[0-9][0-9].json'))
    for json_file in jsonfiles:
        ckip_file = os.path.abspath(json_file).split(os.sep)
        ckip_file.insert(-3, 'CKIP')
        ckip_file = os.sep.join(ckip_file)
        if not os.path.exists(ckip_file):
            print(os.path.abspath(json_file).split(os.sep)[-2:], 'segmentation result does not exist.')
            json_to_ckip(json_file, ckip_file)



def re_segfile():
    """打開json以及對應的ckip檔案，確認ckip檔案內資料是否有缺漏"""
    jsonfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'PChome 購物中心', '*', '*.json'))
    ckipfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'PChome 購物中心', '*', '*_ckip.json'))
    ecfiles = list(set(jsonfiles).difference(set(ckipfiles)))  # 差集：jsonfiles-ckipfiles
    print(ecfiles)

    for ecfile in ecfiles:
        if not os.path.exists(os.path.splitext(os.path.abspath(ecfile))[0] + '_ckip.json'):
            continue
        orgin = json.load(codecs.open(ecfile, 'r', 'utf-8-sig'))
        id_orgin = [article_id for article_id in orgin['Articles']]
        seg = json.load(codecs.open(os.path.splitext(os.path.abspath(ecfile))[0] + '_ckip.json', 'r', 'utf-8-sig'))
        id_seg = [article_id for article_id in seg['Articles']]
        print(list(set(id_orgin).difference(set(id_seg))))

    # 印出差集


def word_frequence(pathlist=[]):
    # if os.path.exists('frequency_byCategory.json'):
    #     return

    stopword_list = stopword()
    word_in_file_frequece = defaultdict(dict)
    for file in pathlist:
        print(file)
        jsoncontent = json.load(codecs.open(file, 'r', 'utf-8'))
        titles_in_file = []
        intros_in_file = []
        format_in_file = []
        detail_in_file = []
        for product_id in jsoncontent['Articles']:
            # 標題
            title_w = newline_join_list(jsoncontent['Articles'][product_id]['Title_wordform'], "")
            title_p = newline_join_list(jsoncontent['Articles'][product_id]['Title_pos'], "")
            title = list(zip(title_w, title_p))
            # 簡介
            intro_w = newline_join_list(jsoncontent['Articles'][product_id]['Intro_wordform'], "")
            intro_p = newline_join_list(jsoncontent['Articles'][product_id]['Intro_pos'], "")
            intro = list(zip(intro_w, intro_p))
            # 規格
            format_w = newline_join_list(jsoncontent['Articles'][product_id]['Format_wordform'], "")
            format_p = newline_join_list(jsoncontent['Articles'][product_id]['Format_pos'], "")
            format = list(zip(format_w, format_p))
            # 詳細資訊
            detail_w = newline_join_list(jsoncontent['Articles'][product_id]['Detail_wordform'], "")
            detail_p = newline_join_list(jsoncontent['Articles'][product_id]['Detail_pos'], "")
            detail = list(zip(detail_w, detail_p))

            # print(list(reversed(list(enumerate(title)))))
            for index, item in reversed(list(enumerate(title))):
                if item[0] in stopword_list['term'] or item[1] in stopword_list['pos']:
                    title_p.pop(index)
                    title_w.pop(index)
            titles_in_file.extend(title_w)
            # print(list(zip(title_w,title_p)))
            for index, item in reversed(list(enumerate(intro))):
                if item[0] in stopword_list['term'] or item[1] in stopword_list['pos']:
                    intro_p.pop(index)
                    intro_w.pop(index)
            intros_in_file.extend(intro_w)
            for index, item in reversed(list(enumerate(format))):
                if item[0] in stopword_list['term'] or item[1] in stopword_list['pos']:
                    format_p.pop(index)
                    format_w.pop(index)
            format_in_file.extend(format_w)
            for index, item in reversed(list(enumerate(detail))):
                if item[0] in stopword_list['term'] or item[1] in stopword_list['pos']:
                    detail_p.pop(index)
                    detail_w.pop(index)
            detail_in_file.extend(detail_w)

        inform_in_file = list(titles_in_file + intros_in_file + format_in_file + detail_in_file)

        # 從詞形過濾符號及網址
        for word in reversed(list(inform_in_file)):
            if len(remove_symbol(word)) == 0 or len(remove_urls(word)) == 0:
                inform_in_file.remove(word)

        word_in_file_frequece[os.path.splitext(os.path.split(file)[-1])[0][:-5]] = Counter(inform_in_file)
        with open('frequency_byCategory.json', 'w', encoding='utf-8') as output:
            json.dump(word_in_file_frequece, output, indent=4, ensure_ascii=False)


def get_feature_set():
    if os.path.exists('featuredict_ckip.json'):
        return

    data = json.load(codecs.open('frequency_byCategory.json', 'r', encoding='utf-8'))

    featureset = set()
    for filename in data:
        for word in data[filename]:
            featureset.update([word])
    # print(featureset)

    feature_dict = defaultdict(dict)
    for index, feature in enumerate(list(featureset)):
        feature_dict[feature] = index

    with open(file='featuredict_ckip.json', mode='w', encoding='utf-8') as output:
        json.dump(feature_dict, output, ensure_ascii=False, indent=4)

        # return featureset


def main():
    # check_segfile()  # 確認資料夾下每個爬蟲檔是否都有斷詞檔
    # ckipfiles = glob.glob(os.path.join(os.path.dirname(__file__), 'PChome 購物中心', '*', '*_ckip.json'))
    # word_frequence(ckipfiles)
    # get_feature_set()
    check_ckipfile()


if __name__ == "__main__":
    # re_segfile()
    main()
