import gensim
from sklearn.cluster import KMeans
from pprint import pprint
import json, codecs

if __name__ == '__main__':
    model = gensim.models.Word2Vec.load("news_all.model")

    keys = model.wv.vocab.keys()

    wordvector = []
    for key in keys:
        wordvector.append(model[key])

    # classCount = 0
    for classCount in range(1, 20):
        clf = KMeans(n_clusters=classCount)
        s = clf.fit(wordvector)
        print(s)
        labels = clf.labels_

        classCollects = {}
        # for i in range(len(keys)):
        #     if labels[i] in classCollects.keys():
        #         classCollects[labels[i]].append(keys[i])
        #     else:
        #         classCollects[labels[i]] = [keys[i]]

        for i, key in enumerate(keys):
            if labels[i] in classCollects.keys():
                classCollects[labels[i]].append(str(key))
            else:
                classCollects[labels[i]] = [str(key)]

        # pprint(classCollects)
        with codecs.open('k_' + str(classCount) + '.json', mode='w', encoding='utf-8-sig') as output:
            json.dump(classCollects, output, ensure_ascii=False, indent=4)