import requests
from datetime import  datetime
import time
import os
import traceback
from test import send

while True:
    try:

        # ubike_ntpe = "https://data.ntpc.gov.tw/od/data/api/54DDDC93-589C-4858-9C95-18B2046CC1FC?$format=json"
        ubike_ntpe= "http://data.ntpc.gov.tw/api/datasets/71CD1490-A2DF-4198-BEF1-318479775E8A/json"
        # 新北市資料，每5分鐘更新一次
        r = requests.get(ubike_ntpe,verify=False)
        open(os.path.join(os.getcwd(),
                          'NTPE_'+datetime.strftime(datetime.now(), '%Y%m%d_%H%M') + '.json'), 'wb').write(r.content)

        print(datetime.now())
        send('NTPE_'+datetime.strftime(datetime.now(), '%Y%m%d_%H%M'))
        time.sleep(300)
    except Exception as e:
        print(traceback.print_exc())
        print(e)