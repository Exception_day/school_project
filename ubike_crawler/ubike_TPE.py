import requests
from datetime import datetime
import time
import os
import traceback
from test import send

while True:
    try:
        ubike_tpe = "https://tcgbusfs.blob.core.windows.net/blobyoubike/YouBikeTP.json"

        # 台北市資料，每分鐘更新
        r = requests.get(ubike_tpe)
        open(os.path.join(os.getcwd(),
                          'TPE_'+datetime.strftime(datetime.now(), '%Y%m%d_%H%M') + '.json'), 'wb').write(r.content)
        print(datetime.now())
        if datetime.now().minute%5==0:
            send('TPE_'+datetime.strftime(datetime.now(), '%Y%m%d_%H%M'))
        time.sleep(60)
    except Exception as e:
        print(traceback.print_exc())
        print(e)